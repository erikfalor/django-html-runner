from django.shortcuts import render

from django.http import HttpResponse


from time import strftime

def index(request):
    now = strftime('%c')
    return HttpResponse("<h1>Hello, world.</h1><p>You're a swell person</p><p>Right now, over here, it is %s</p>" % now)