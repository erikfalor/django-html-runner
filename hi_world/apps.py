from django.apps import AppConfig


class HiWorldConfig(AppConfig):
    name = 'hi_world'
